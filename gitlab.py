Status | git status
History | git log
Clone project to repository | git clone <Clone with HTTPS>
Add File to repository | git add <filename>
Publish code back to Gitlab | git push
Download other users' changes | git pull
Add changes to repository | git commit -m"my active tone commit message"
Open current directory in VS Code | code .
Track changes in VS Code | git add . OR git add <filename>
