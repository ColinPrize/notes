# SPELLCHECK TERMS
example_dict = {}
# SPELLCHECK TERMS



ADD/UPDATE KEY-VALUES IN DICT
    example_dict["key"] = "value"

REMOVE KEY-VALUES FROM DICT
    del example_dict["key"]

POP REMOVE METHOD
    exmaple_dict.pop("key")

POP LAST ADDED KEY-VALUE
    example_dict.popitem()

CLEAR DICT CONTENTS
    example_dict.clear()

FIND LENGTH OF DICT
    len(example_dict)

GET METHOD
    x = example_dict.get("key")

GET WITH BRACKETS
    x = example_dict["key"]

LIST ALL KEYS
    x = example_dict.keys()

LIST ALL VALUES
    x = example_dict.values()

LIST ALL KEY-VALUE TUPLES
    x = example_dict.items()





def make_description(name, attributes):
    result = name
    
    for key, value in attributes.items():
        result += key + value
    return result
