from django.shortcuts import render, get_object_or_404
from examples.models import Example

# VIEW SINGLE
def show_model(request, id):
    example = get_object_or_404(Example, id=id)   # Get the data
    context = {   # Put the data in a context
        "example_object": example,
    }
    return render(request, "example/detail.html", context)   # Render HTML with context data

# VIEW ALL
def list_models(request):
    example = Example.objects.all()
    context = {
        "allexamples_object": example,
    }
    return render(request, "example/list.html", context)
    

# EDIT EXISTING
def edit_example(request, id)
    model_instance = ModelName.objects.get(pk=pk)
    if request.method == "POST":
        form = ModelForm(request.POST, instance=model_instance)
        if form.is_valid():
            form.save()
            # If you need to do something to the model before saving,
            # you can get the instance by calling
            # model_instance = form.save(commit=False)
            # Modifying the model_instance
            # and then calling model_instance.save()
            return redirect("some_url")
    else:
        form = ModelForm(instance=model_instance)

    context = {
        "form": form
        }

    return render(request, "Example_names/edit.html", context)


# DELETE EXISTING
def delete_example(request, id)
    model_instance = ModelName.objects.get(pk=pk)
    if request.method == "POST":
        model_instance.delete()
        return redirect("some_url")

    return render(request, "Examples_names/delete.html")


# CREATE NEW
def create_example(request)
    if request.method == "POST":
        form = ModelForm(request.POST)
        if form.is_valid():
            form.save()
            # If you need to do something to the model before 
            # saving, you can get the instance by calling
            # model_instance = form.save(commit=False)
            # Modifying the model_instance
            # and then calling model_instance.save()
            return redirect("some_url")
    else:
        form = ModelForm()

    context = {
        "form": form
        }

    return render(request, "Example_model/create.html", context)
