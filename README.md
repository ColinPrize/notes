## ToDoList
    NOTE_MAKING paused at D5: Lists and Forms > HTML Forms
    DJANGO TWOSHOT paused at Feature 5 - Receipt ListView
    Add 3 reamining view functions - Shahzad AM lecture 10/17
    Fill in Psudoclass section in css file
    Reformat python_basics
    Flesh out paths.py


## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)
