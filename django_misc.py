# SPELLCHECK TERMS
Example_model = {}
# SPELLCHECK TERMS

FILTER METHOD
	Example_model.objects.filter(title="ItemName")

GET OBJECT IN TABLE AT ID X
	Example_model.objects.get(id=X)

GET OBJECT BY PRIMARY KEY
	Example_model.objects.get(pk=X)

GET OBJECT BY SOME FIELD
	Example_model.objects.get(some_field="field value")	

GET ALL OBJECTS
	Example_model.objects.all()

SERVER
	start | python manage.py runserver
	stop | control+C

REGISTER MODEL WITH ADMIN PAGE
	admin.site.register(<<model_name>>)

DEPENDENCIES
    create list | pip freeze > requirements.txt
    look at list | pip freeze

APP REQUEST NAMES
	Http | Crud | SuccessCode : description
	Post | Create | 201 : create new resources. In particular subordinate resources
	Get | Read | 200 : retrieve a representation of a resource
	Put | Update/Replace | 204 : updating capabilities, PUT-ing to a known resource URI
	Patch | Update/Modify | 204 : modify capabilities. PATCH only needs to contain changes to the resource, not the complete resource
	Delete | Delete | 204 | : deletes a resource identified by a URI

PROJECT DIRECTORY FILES
-   ___init__.py_, used for Python to make the directory a loadable module
-   _asgi.py_, used to start the Django Web application using a 3rd-party asynchronous Web server
-   _settings.py_, the collection of all the necessary settings for the Django application to happily run
-   _urls.py_, the principal file that Django uses to configure the paths that it will match to when it receives an HTTP request
-   _wsgi.py_, used to start the Django Web application using a third-party Web server, like **gunicorn**, the Web server we used to deploy our Django application

APP DIRECTORY FILES
-   ___init__.py_, the file used for Python to make the directory a loadable module
-   _admin.py_, the file that allows you to register your models with the Admin site
-   _apps.py_, the file that defines your Django application. It allows you to configure this specific Django application, as well as run functions of yours when it loads or other events occur
-   _migrations/_, the directory that holds any migrations for the models defined in this Django application.
-   _models.py_, where you define the classes that describe the structure and relationships of data that you have
-   _tests.py_, a file that can hold tests
