# SPELLCHECK TERMS
project_name = ""
app_name = ""
function_name = ""
Example_model = ""
# SPELLCHECK TERMS



[1] START VIRTUAL ENVIRONMENT
	Create Mac | python -m venv ./.venv
	Create Win | ./.venv/Scripts/Activate.ps1 
	Activate | source ./.venv/bin/activate
	Exit |  deactivate

[2] PIP INSTALL DJANGO
	pip install django

[3] PIP INSTALL DEPENDENCIES
	pip install -r requirements.txt
	
[4] START DJANGO PROJECT
	django-admin startproject <<project_name>> .

[5] GENERATE A DJANGO APP
	python manage.py startapp <<app_name>>
	
[6] INSTALL APP IN SETTINGS.PY (in VS Code)
	Add app_name from apps.py to Installed Apps list

[7] MIGRATIONS
	python manage.py makemigrations
	python manage.py migrate

[8] WRITE FUNCTIONS IN VIEWS.PY
	view_single
	view_all
	edit_existing
	delete_existing
	create_new

[9] CREATE URLS.PY IN APP DIRECTORY
	from django.urls import path
	from app_name.views import function_name

	urlpatterns = {
		path("app_name/<int:id>/", function_name),
	}

[10] INCLUDE APP URL.PY IN PROJECT URL.PY
	from django.urls import path, include

	urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("app_name.urls")),
	]

[11] SET UP HTML TEMPLATES
	in app directory, create a templates directory
	in templates directory, create a app_name directory
	in app_name directory, create html files
	add content to html files
	
[12] SET UP STATIC CSS
	in app directory, create a static directory
	in static directory, create a css directory
	in css directory, create app_name.css
	add content to css files

[13] LOAD STATIC AT TOP OF HTML FILES
	{% load static %}

[14] CHANGE LINK TAG TO STATIC VERSION
	<link rel="stylesheet" href="{% static "css/app_name.css" %}">

[15] WRITE MODELS IN MODELS.PY
	class Example_model(models.Model):
		attribute = models.DjangoFieldType()
		name = models.CharField(max_length=100)
		image = models.URLField()
		description = models.TextField()
		created_on = models.DateTimeField(auto_now_add=True)
		rating = models.FloatField(null=True)

[16] CREATE ADMIN USER
	python manage.py createsuperuser
	[http://localhost:8000/admin/]

[17] REGISTER EXAMPLE_MODEL IN ADMIN.PY
	from app_name.models import Example_model

	@admin.register(Example_model)
	class Example_modelAdmin(admin.ModelAdmin):
		list_display = (
			"title",
			"id",
		)

[18] CREATE EXAMPLE_MODEL DATA
	In admin page, press +Add button to make a bunch of the model

[19] CHANGE MORE HTML TAGS TO STATIC VERSION
	title | <title>{{ example_object.title }}</title>
	img | <img src="{{ example_object.picture }}">
	p | <p>{{ example_object.description }}</p>
	p | <p>Rated: {{ example_object.rating }} out of 5</p>
	etc

[20]  
